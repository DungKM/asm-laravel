<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductDetail;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $category;
    protected $product;
    protected $productDetail;

    public function __construct(Product $product, Category $category, ProductDetail $productDetail)
    {
        $this->product = $product;
        $this->category = $category;
        $this->productDetail = $productDetail;
    }
    public function index(Request $request)
    {
        $search = $request->get(key:'q');
        $products = $this->product->latest('id')->where(column:'name', operator:'like', value:'%'.$search.'%')->paginate(3);
        $products->load('category');
        return view('admin.product.index', compact('products','search'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = $this->category->get(['id', 'name']);
        return view('admin.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $dataCreate = $request->except('sizes');
        $sizes = $request->sizes ? json_decode($request->sizes) : [];
        $dataCreate['image'] = $request->file('image')->getClientOriginalName(); //lay ten file
        $request->file('image')->storeAs('public/images', $dataCreate['image']); //luu file vao duong dan public/images voi ten $logo

        $product = $this->product->create($dataCreate); //tao ban ghi co du lieu la $data
        $sizeArray = [];
        foreach ($sizes as $size) {
            $sizeArray[] = ['size' => $size->size, 'quantity' => $size->quantity, 'product_id' => $product->id];
        }
        $this->productDetail->insert($sizeArray);
        return redirect()->route('products.index')->with('success', 'Product has been created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $product = $this->product->with(['details', 'category'])->findOrFail($id);
        $product->description = htmlspecialchars_decode($product->description);

        return view('admin.product.show', compact('product'));
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $product = $this->product->with(['details', 'category'])->findOrFail($id);

        $categories = $this->category->get(['id', 'name']);
        return view('admin.product.edit', compact('categories','product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $dataUpdate = $request->except('sizes');
        $sizes = $request->sizes ? json_decode($request->sizes) : [];
        $product = $this->product->findOrFail($id);
        if ($request->file('image') !== null) {
            $dataUpdate['image'] = $request->file('image')->getClientOriginalName(); //lay ten file
            $request->file('image')->storeAs('public/images',   $dataUpdate['image']); //luu file vao duong dan public/images voi ten $image
        }
        $product->update( $dataUpdate); //tao ban ghi co du lieu la $data
        
        $sizeArray = [];
        foreach ($sizes as $size) {
            $sizeArray[] = ['size' => $size->size, 'quantity' => $size->quantity, 'product_id' => $product->id];
        }
        $product->details()->delete();
        $this->productDetail->insert($sizeArray);
        return redirect()->route('products.index')->with('success', 'Product has been updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')
            ->with('success', 'Delete products successfully');
    }
}