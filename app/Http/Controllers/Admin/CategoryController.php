<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    protected $category;
    public function __construct(Category $category)
    {
        $this->category = $category;
    }
    public function index()
    {
        $categories =  $this->category->latest('id')->paginate(5);
        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategoryRequest $request)
    {
        $name = $request->input('name'); //lay name tu request form gui len

        $image = $request->file('image')->getClientOriginalName(); //lay ten file
        $request->file('image')->storeAs('public/images', $image); //luu file vao duong dan public/images voi ten $logo

        //tao data de luu vao db
        $data = [
            'name' => $name,
            'image' => $image,
        ];

        $this->category->create($data); //tao ban ghi co du lieu la $data

        return redirect()->route('category.index')
            ->with('success', 'Category has been created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $category = $this->category->findOrFail($id);
        return view('admin.category.edit', compact('category'));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $name = $request->input('name'); // lay input name moi
        $category->fill([
            'name' => $name,
        ])->save();

        if ($request->file('image') !== null) {
            $image = $request->file('image')->getClientOriginalName(); //lay ten file
            $request->file('image')->storeAs('public/images', $image); //luu file vao duong dan public/images voi ten $image

            $category->fill([
                'image' => $image,
            ])->save();
        }

        return redirect()->route('category.index')
            ->with('success', 'Category update successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('category.index')
            ->with('success', 'Delete category successfully');
    }
}