<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product->latest('id')->paginate(5);

        return view('client.shop.index', compact('products'));
    }

    public function category_product(Request $request, $category_id)
    {
        $products = $this->product->getBy($request->all(), $category_id);
        return view('client.shop.index', compact('products'));
    }
    public function show($id)
    {
        $product = $this->product->with('details')->findOrFail($id);
        $categoryId = $product->category->pluck('id')->first(); // Assuming a product can belong to multiple categories, we retrieve the first category ID.

        $relatedProducts = $this->product->getBy($product->name, $categoryId);

        return view('client.shop.detail', compact('product', 'relatedProducts'));
    }
}