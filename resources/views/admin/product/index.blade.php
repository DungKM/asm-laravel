@extends('admin.layouts.app')
@section('title', 'Product')

@section('content')
    <div class="card">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card-body">
            <h4 class="card-title">Product</h4>
            <p class="card-description">
                <a href="{{ route('products.create') }}" class="btn btn-info font-weight-bold">+ Create Product</a>
            </p>
            <div class="pull-left search">
                <form>
                    <input type="search" class="form-control" placeholder="Search" name="q"
                        value="{{ $search }}">
                </form>
            </div>
            <div class="table-responsive pt-3">
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                ImageProduct
                            </th>
                            <th>
                                NameProduct
                            </th>
                            <th>
                                Price

                            </th>
                            <th>
                                NameCategory
                            </th>
                            <th>
                                Action
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $item)
                            <tr>
                                <td>
                                    {{ $item->id }}
                                </td>
                                <td>
                                    <img src="{{ asset('/storage/images/' . $item->image) }}" width="100">
                                </td>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td>
                                    {{ $item->price }}
                                </td>
                                <td>
                                    {{ $item->category->name }}
                                </td>

                                <td>
                                    <form action="{{ route('products.destroy', $item->id) }}" method="post">
                                        <a href="{{ route('products.edit', $item->id) }}" class="btn btn-info">Update</a>
                                        <a href="{{ route('products.show', $item->id) }}" class="btn btn-info">Show</a>
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="btn-group mt-1" role="group" aria-label="Basic example">
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
