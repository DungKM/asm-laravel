@extends('admin.layouts.app')
@section('title', 'Users')
@section('content')
    <div class="card">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card-body">
            <h4 class="card-title">USER</h4>
            <div class="pull-right mb-2">
                <a class="btn btn-success" href="{{ route('users.create') }}"> New user</a>
            </div>
            <div class="table-responsive pt-3">
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>

                            <th>
                                Email
                            </th>
                            <th>
                                Phone
                            </th>
                            <th>
                                Gender
                            </th>
                            <th>
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $item)
                            <tr>
                                <td>
                                    {{ $item->id }}
                                </td>
                                <td>
                                    {{ $item->name }}
                                </td>

                                <td>
                                    {{ $item->email }}
                                </td>
                                <td>
                                    {{ $item->phone }}
                                </td>
                                <td>
                                    {{ $item->gender }}
                                </td>
                                <td>
                                    <div class="row">
                                        <form action="{{ route('users.destroy', $item->id) }}" method="post">
                                            <a href="{{ route('users.edit', $item->id) }}"
                                                class="btn btn-info btn-fw">Update</a>
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger">Delete</button>
                                        </form>

                                    </div>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                <div class="btn-group mt-1" role="group" aria-label="Basic example">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
