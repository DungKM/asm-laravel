@extends('admin.layouts.app')
@section('title', 'Edit Category')


@section('content')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">EDIT CATEGORY</h4>
            <p class="card-description">
                Back
            </p>
            <form class="forms-sample" action="{{ route('category.update', $category->id) }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="exampleInputName1">Name</label>
                    <input type="text" class="form-control" name="name" id="exampleInputName1"
                        value="{{ old('name') ?? $category->name }}" placeholder="Name">
                </div>
                @error('name')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>File upload</label>
                    <input type="file" name="image" value="{{ $category->image }}" class="file-upload-default" id="image-input">
                    <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled="" value="{{ $category->image }}"
                            placeholder="Upload Image">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                    </div>
                    <img src="{{ asset('/storage/images/' . $category->image) }}" style="height: 50px;width:100px;" id="show-image">
                </div>
                @error('image')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-info">Submit</button>

            </form>
        </div>
    </div>

@endsection
@push('script')
<script src="{{ asset('admin/template/product/product.js') }}"></script>
@endpush

