@extends('admin.layouts.app')
@section('title', 'Category')


@section('content')
    <div class="card">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card-body">
            <h4 class="card-title">Category</h4>
            <div class="pull-right mb-2">
                <a class="btn btn-success" href="{{ route('category.create') }}"> New brand</a>
            </div>
            <div class="table-responsive pt-3">
                <table class="table table-dark">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Image
                            </th>
                            <th>
                                Action
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $item)
                            <tr>
                                <td>
                                    {{ $item->id }}
                                </td>
                                <td>
                                    {{ $item->name }}
                                </td>
                                <td>
                                    <img src="{{ asset('/storage/images/' . $item->image) }}" width="100" >
                                </td>
                                <td>
                                    <form action="{{ route('category.destroy', $item->id) }}" method="Post">
                                        <a class="btn btn-primary" href="{{ route('category.edit', $item->id) }}">Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
                <div class="btn-group mt-1" role="group" aria-label="Basic example">
                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
