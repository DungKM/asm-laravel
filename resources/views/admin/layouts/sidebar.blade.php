 <!-- partial:partials/_sidebar.html -->
 <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="mdi mdi-grid-large menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('roles.index')}}">
          <i class="mdi mdi-account-key menu-icon"></i>
          <span class="menu-title">Roles</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('products.index')}}">
          <i class="mdi mdi-carrot menu-icon"></i>
          <span class="menu-title">Product</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('category.index')}}">
          <i class="mdi mdi-cards-playing-outline menu-icon"></i>
          <span class="menu-title">Category</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="mdi mdi-cart-outline menu-icon"></i>
          <span class="menu-title">Cart</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="mdi mdi-cart-plus menu-icon"></i>
          <span class="menu-title">Order</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="mdi mdi-comment-text-outline menu-icon"></i>
          <span class="menu-title">Comment</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('users.index')}}">
          <i class="mdi mdi-account-multiple-outline menu-icon"></i>
          <span class="menu-title">User</span>
        </a>
      </li>
    
    </ul>
  </nav>