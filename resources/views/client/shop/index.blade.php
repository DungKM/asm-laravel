@extends('client.layouts.app')
@section('content')
<div class="colorlib-featured">
    <div class="container">
        <div class="row">
            @foreach ($categories as $item)
            <div class="col-sm-4 text-center">
                <div class="featured">
                    <div class="featured-img featured-img-2" style="background-image: url({{ asset('/storage/images/' . $item->image) }});">
                        <h2>{{$item->name}}</h2>
                        <p><a href="{{ route('client.products.categoryid', ['category_id' => $item->id]) }}" class="btn btn-primary btn-lg">Shop now</a></p>
                    </div>
                </div>
            </div>
            @endforeach
           
         
        </div>
    </div>
</div>

<div class="colorlib-product">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-xl-3">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="side border mb-1">
                            <h3>Brand</h3>
                            <ul>
                                <li><a href="#">Nike</a></li>
                                <li><a href="#">Adidas</a></li>
                                <li><a href="#">Merrel</a></li>
                                <li><a href="#">Gucci</a></li>
                                <li><a href="#">Skechers</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="side border mb-1">
                            <h3>Size/Width</h3>
                            <div class="block-26 mb-2">
                                <h4>Size</h4>
                                <ul>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">7.5</a></li>
                                    <li><a href="#">8</a></li>
                                    <li><a href="#">8.5</a></li>
                                    <li><a href="#">9</a></li>
                                    <li><a href="#">9.5</a></li>
                                    <li><a href="#">10</a></li>
                                    <li><a href="#">10.5</a></li>
                                    <li><a href="#">11</a></li>
                                    <li><a href="#">11.5</a></li>
                                    <li><a href="#">12</a></li>
                                    <li><a href="#">12.5</a></li>
                                    <li><a href="#">13</a></li>
                                    <li><a href="#">13.5</a></li>
                                    <li><a href="#">14</a></li>
                                </ul>
                            </div>
                            <div class="block-26">
                                <h4>Width</h4>
                                <ul>
                                    <li><a href="#">M</a></li>
                                    <li><a href="#">W</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="side border mb-1">
                            <h3>Style</h3>
                            <ul>
                                <li><a href="#">Slip Ons</a></li>
                                <li><a href="#">Boots</a></li>
                                <li><a href="#">Sandals</a></li>
                                <li><a href="#">Lace Ups</a></li>
                                <li><a href="#">Oxfords</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="side border mb-1">
                            <h3>Colors</h3>
                            <ul>
                                <li><a href="#">Black</a></li>
                                <li><a href="#">White</a></li>
                                <li><a href="#">Blue</a></li>
                                <li><a href="#">Red</a></li>
                                <li><a href="#">Green</a></li>
                                <li><a href="#">Grey</a></li>
                                <li><a href="#">Orange</a></li>
                                <li><a href="#">Cream</a></li>
                                <li><a href="#">Brown</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="side border mb-1">
                            <h3>Material</h3>
                            <ul>
                                <li><a href="#">Leather</a></li>
                                <li><a href="#">Suede</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="side border mb-1">
                            <h3>Technologies</h3>
                            <ul>
                                <li><a href="#">BioBevel</a></li>
                                <li><a href="#">Groove</a></li>
                                <li><a href="#">FlexBevel</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-xl-9">
                <div class="row row-pb-md">
                    @foreach ($products as $item)
                    <div class="col-lg-4 mb-4 text-center">
                        <div class="product-entry border">
                            <a href="#" class="prod-img">
                                <img src="{{ asset('/storage/images/' . $item->image) }}" class="img-fluid" alt="Free html5 bootstrap 4 template">
                            </a>
                            <div class="desc">
                                <h2><a href="{{ route('client.product.show', $item->id) }}">{{$item->name}}</a></h2>
                                <span class="price">${{$item->price}}</span>
                            </div>
                        </div>
                    </div>
                    
                    @endforeach
                
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="block-27">
                            <ul>
                                @if ($products->currentPage() > 1)
                                    <li><a href="{{ $products->previousPageUrl() }}"><i class="ion-ios-arrow-back"></i></a></li>
                                @endif
                    
                                @for ($page = 1; $page <= $products->lastPage(); $page++)
                                    @if ($page == $products->currentPage())
                                        <li class="active"><span>{{ $page }}</span></li>
                                    @else
                                        <li><a href="{{ $products->url($page) }}">{{ $page }}</a></li>
                                    @endif
                                @endfor
                    
                                @if ($products->hasMorePages())
                                    <li><a href="{{ $products->nextPageUrl() }}"><i class="ion-ios-arrow-forward"></i></a></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
</div>

@endsection
